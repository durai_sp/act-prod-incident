"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Oct 09, 2017

handle functionality is used to initiate act prod incident.
"""

from dashboard import Dashboard
import collections


dd = {"acnEnabled":"boolean", "acnOpted":"boolean", "clientIncidentID":"string", "incidentID":"string", "incidentType":"string", "insertLocalTime":"string", "insertTime":"int", "latitude":"float", "longitude":"float", "make":"string", "model":"string", "tripID":"string", "userID":"string", "year":"string", "isAccident":"string", "isAmbReq":"string", "responseType":"string", "modifyLocalTime":"string", "modifyTime":"int", "vendorIncidentID":"string", "crashConfirmed":"string", "notificationSent":"string", "e911Status":"string", "roadSideService":"string", "roadSideServiceStatus":"string", "processedDateUtcTTL":"int", "processedTimeUtc":"int"}

dashboard = Dashboard('act-agero-prod-incident', 'act-agero-prod-incident', 'us-east-1', 'DEBUG')  
dashboard.handler(dd)



