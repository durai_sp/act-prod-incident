"""
SES connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: May 31, 2017

SESUtils class has the necessary functions to send email.
"""

import boto3
import logging_helper as Log
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

import os

class SESUtils():
    def __init__(self, level):
        self.logs = Log.log(level)
        self.logs.info('SES Utils initialized')
        self.ses_client = boto3.client('ses')

    def send_email_cc_bcc(self, from_email, to_email, cc_email, bcc_email, subject, body, html):
        """This method is used to send email with cc and bcc details.

        :param from_email: Provide the senders email.
        :param to_email: Provide the receivers email or list of emails.
        :param cc_email: Provide the cc'ed email or list of emails.
        :param bcc_email: Provide the bcc'ed email or list of emails.
        :param subject: Provide the subject.
        :param body: Provide the body.
        :param html: Provide the html string.
        :return This method returns a the response of the sent email.
        """
        response = self.ses_client.send_email(
            Source=from_email,
            Destination={
                'ToAddresses': [
                    to_email,
                ],
                'CcAddresses': [
                    cc_email,
                ],
                'BccAddresses': [
                    bcc_email,
                ]
            },
            Message={
                'Subject': {
                    'Data': subject,
                    'Charset': 'UTF-8',
                },
                'Body': {
                    'Text': {
                        'Data': body,
                        'Charset': 'UTF-8',
                    },
                    'Html': {
                        'Data': html,
                        'Charset': 'UTF-8',
                    }
                }
            }
        )
        return response

    def send_email_without_cc_bcc(self, from_email, to_email, subject, body, html):
        """This method is used to send email without cc and bcc details.

        :param from_email: Provide the senders email.
        :param to_email: Provide the receivers email or list of emails.
        :param subject: Provide the subject.
        :param body: Provide the body.
        :param html: Provide the html string.
        :return This method returns a the response of the sent email.
        """
        response = self.ses_client.send_email(
            Source=from_email,
            Destination={
                'ToAddresses': [
                    to_email,
                ],
                'CcAddresses': [
                ],
                'BccAddresses': [
                ]
            },
            Message={
                'Subject': {
                    'Data': subject,
                    'Charset': 'UTF-8',
                },
                'Body': {
                    'Text': {
                        'Data': body,
                        'Charset': 'UTF-8',
                    },
                    'Html': {
                        'Data': html,
                        'Charset': 'UTF-8',
                    }
                }
            }
        )
        return response

    def send_email_with_attachment(self, from_email, to_email, file_path, subject, text):
        """This method is used to send email with an attachment.

        :param from_email: Provide the senders email.
        :param to_email: Provide the receivers email or list of emails.
        :param file_path: Provide the file path for the file to attach (/Users/XYZ/abc.pdf).
        :param subject: Provide the subject of th email.
        :param text: Provide the text to include in the body.
        :return This method returns a the response of the sent email.
        """
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = from_email
        part = MIMEText(text)
        msg.attach(part)

        part = MIMEApplication(open(file_path, "rb").read())
        part.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file_path))
        msg.attach(part)
        response = self.ses_client.send_raw_email(
            RawMessage={
                'Data': msg.as_string(),
            },
            Source=msg['From'],
            Destinations=to_email)
        return response